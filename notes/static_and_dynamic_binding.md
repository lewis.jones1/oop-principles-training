# Static and Dynamic Binding

## Static Binding

Binding which can be resolved at compile time by the compiler, e.g. the binding of static, private and final methods. These methods **cannot be overridden** and the type of the class is determined at compile time.

### Example

Two classes, Human and Boy. Both classes have the `walk()` method, but it is static, meaning it cannot be overridden. 

```java
public class Human {
  public static void walk() {
    System.out.println("Human walks");
  }
}

public class Boy extends Human {
  public static void walk() {
    System.out.println("Boy walks");
  }

  public static void main(String args[]) {
    // Declare a new Boy object, but the reference is Human
    Human obj = new Boy();

    // Declare new Human object with reference as Human
    Human obj1 = new Human();

    obj.walk();
    obj1.walk();

    // output
    // Human walks
    // Human walks
  }
}
```

The output is `Human walks` for both, because the **reference for both is set to Human**. Static methods belong to the Reference Class and cannot be overridden.

## Dynamic Binding

When a compiler cannot resolve the call/binding at compile time, this is known as dynamic binding. Method Overriding is a good example. The parent and child classes both contain the same method, but the **type of the object** determines which is executed.

### Example

```java
public class Human {
  public void walk() {
    System.out.println("Human walks");
  }
}

public class Boy extends Human {
  public void walk() {
    System.out.println("Boy walks");
  }

  public static void main(String args[]) {
    // Declare a new Boy object, reference in Boy
    Boy obj = new Boy();

    // Declare new Human object with reference as Human
    Human obj1 = new Human();

    obj.walk();
    obj1.walk();

    // output
    // Boy walks
    // Human walks
  }
}
```

In this example, the methods are **not** static, private or final. The type of an object is determined at runtime, meaning that which method (either Boy or Human) is decided at runtime based on what type the object is.

## Review

1. Static binding happens at compile-time, dynamic binding happens at runtime.
2. Binding of private, static and final methods **always happen at compile time**, since these methods cannot be overridden.
3. Binding of overloaded methods is static, and the binding of overridden methods is dynamic.
