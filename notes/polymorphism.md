# Polymorphism

This allows us to perform single action in different ways. We can provide different implementations to the same method, which can be annotated using the `@Override` annotation.

```java
public class Animal {
  ...
  public void sound() {
    System.out.println("Animal is making a sound");
  }
}

public class Dog extends Animal {
  @Override
  public void sound() {
    System.out.println("Woof!");
  }
}
```

Polymorphism lets a method do different things based on the object that it is acting upon. It allows you to define **one interface** and have **multiple implementations**. The above example is **runtime polymorphism**.

## Method Overloading - compile time

This is a feature that allows a class to have more than one method with the same name, if their argument lists are different. Similar to constructor overloading, where a class can have more than one constructor if the argument list is different.

Overloading can increase readability of a program, by giving programmers the flexibility to call a similar method for different types of data. It means that the developer does not need to write lots of different method names to do the same thing, just because the parameter lists differ.

## Method Overriding - runtime

Declaring a method in sub class which is already present in parent class is known as method overriding. It is done so that a child class can give its own implementation to a method, which is already provided by the parent class. It could be good for giving a default/standard result, which then can be overriden by the child class.

Main advantage of overriding is that the class can have specific implementation to an inherited method without needing to modify the parent class code. This is helpful when a class has several child classes.

## Runtime Polymorphism example

see animal example above.

## Compile time Polymorphism example

Method overloading is an example of compile time polymorphism.

```java
public class Overload {
  public void demo(int a) {
    System.out.println("a: " + a);
  }

  public void demo(int a, int b) {
    System.out.println("a and b: " + a + ", " +  b);
  }

  public void demo(double a) {
    System.out.println("double a: " + a);
  }
}
```

`demo()` is overloaded 3 times, which method to be called is determined by the arguments that we pass whilst calling methods, so this happens at **compile time**.
