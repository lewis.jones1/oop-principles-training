# Abstraction

This is the process of hiding certain details and showing only essential information to the user. Can be achieved with either abstract classes or interfaces. The `abstract` keyword is a non-access modifier, used for classes and methods.

## Abstract Method

This is simply a declaration of a method that does not have a body. When a class has an abstract method, the class **must be declared as abstract**.

## Abstract Class

Restricted class that cannot be used to create objects - must be inherited from another class. An abstract class can have both abstract and regular methods, but it **cannot be instantiated**. Animal class with a method `sound()` - each animal has a different sound, so there is no need to implement this method in the parent class. Every child class _must_ override this method and give its own implementation details.

When we know that every child class will and should override a method, there is no point implementing this method in the aprent class. Therefore, making it abstract would be a good choice, as it forces all sub classes to implement the method.

### Example

```java
public abstract class Animal() {
  public abstract void sound();
}

public class Dog extends Animal() {
  public void sound() {
    System.out.println("Woof!");
  }
}
```

Dog is a **concrete class**, as it extends the abstract class and overrides any methods accordingly.

### Declaration

It outlines the methods, but does not necessarily implement all of the methods (although it can implement some of the methods). There are sometimes cases where it is difficult or unecessary to implement all methods in a parent class, because they will be overridden by subclasses. A subclass must implement all abstract methods.

**Abstract classes cannot be instantiated**. This is because the classes are incomplete - cannot invoke the method if the implementation has not been added. An abstract class is like a template which can be extended and built upon.

# Interfaces

Interfaces are used for full abstraction. All methods of an interface are public abstract by default. Cannot have concrete methods in an interface. e.g.

```java
public interface Multiply {
  // Do not need to say public or abstract in interface
  // Compiler automatically knows that this is implied

  int multiplyTwo(int n1, int n2);
  int multiplyThree(int n1, int n2, int n3);
}

// Implements the interface in a concrete class
public class Demo implements Multiply {
  public int multiplyTwo(int n1, int n2) {
    return n1*n2;
  }

  public int multiplyThree(int n1, int n2, int n3) {
    return n1*n2*n3;
  }
}
```

Interfaces look like a class, but they are not the same. They can have methods and variables, but the methods declared are by default abstract. Variables are public, static and final by default.

## Why use interfaces?

Full abstraction!

Since methods do not have a body, they must be implemented before they can be accessed. The class that implements an interface must implement all methods of the interface. Multiple interfaces can be implemented in a class.

Also, it means that someone can be given an interface to know what methods can be called, but they do not need to know the implementation of those methods i.e. it is **hidden** from them.

## Interface and Inheritance

An interface cannot implement another interface - it has to **extend** another interface.

```java
interface Inf1 {
  public void method1();
}

interface Inf2 extends Inf1 {
  public void method2();
}

public class Demo implements Inf2 {
  // It must implement both methods from Inf1 and Inf2
  public void method1() {
    // implementation
  }

  public void method2() {
    // implementation
  }
}
```

## Key points about interfaces

* Cannot instantiate an interface
* Provides full abstraction as none of its methods have bodies. Abstract classes provide partial abstraction as it can have concrete methods
* When implementing an interface method in a class, it needs to be mentioned as public
* Classes that implement an interface must implement all methods of that interface, or they should be declared abstract
* interface methods are by default abstract and public
* interface variables are public, static and final and must be initialised at declaration