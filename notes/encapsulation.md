# Encapsulation

Mechanism of wrapping variables and methods together as a single unit. The variables will be hidden from other classes and can only be accessed through methods of their own class. Need to declare variables as private and provide public getter and setter methods to modify and view specific variable values.

The benefits of encapsulation are that fields of a class can be made read-only or write-only, and a class can have total control over what is stored in its fields, whilst an outside class cannot access values that it shouldn't.

## Key characteristics of encapsulated code

* Users know how to access it
* Can easily be used, regardless of implementation details
* Shouldn't have any side effects of the code on the rest of the application
* Classes are kept separated and they are prevented from being tightly coupled with each other

## How to implement in Java

1. Make instance variables private, so they cannot be accessed directly from outside the class
2. Have getter and setter methods

## Advantages

1. It improves maintainability, flexibility and reusability. The implementation of the getters or setters can be changed at any point in time, because the implementation is hidden for outside classes.
2. Fields can be made read-only by not declaring setters in the class, or write-only by not declaring getters
3. User is not aware of implementation code - only know that to update a specific field, the setter can be called, and to read the field, the getter can be used. The implementation is hidden from users at all times
