# Introduction

[Reference](https://beginnersbook.com/2013/04/oops-concepts/)

The primary purpose of OOP is to increase the **flexibility** and **maintainablility** of programs. OOP brings together data and its behaviour (methods) in a single location (object) and makes it easier to understand how a program works.

## Objects

An object is an instance of a class. It is a bundle of data and its behaviour, and they have states and behaviours. States are represented as instance variables and behaviours as methods of the class.

### e.g. House

* Object = House
* State = Address, Color, Area
* Behaviour: Open door, Close door

Example of how this might be represented in Java below:

```java
class House {
  String address;
  String color;
  double area;
  
  void openDoor() {
    // implementation
  }

  void closeDoor() {
    // implementation
  } 
}
```

## What is a class in OOPs

A blueprint which can be used to create objects. Classes do not actually represent any specific object, but the class can be used to create instances, which actually represents something.

### e.g. a simple Website

```java
public class Website {
  String webName;
  int webAge;

  // Constructor
  public Website(String name, int age) {
    this.webName = name;
    this.webAge = age;
  }

  public static void main(String args[]) {
    // Creating objects
    Website obj1 = new Website('Google', 36);
    Website obj2 = new Website('Facebook', 18);

    // Accessing object data
    System.out.println(obj1.webName + " " + obj1.webAge); // output: "Google 36"
    System.out.println(obj2.webName + " " + obj2.webAge); // output: "Facebook 18"
  }
}
```

## Object Oriented Programming features

This section will provide an overview of the four OOP principles, and more detailed notes can be found in other markdown files.

### Abstraction

This is a process where only "relevant" data is shown to a user, and unnecessary details are "hidden". e.g. when logging into a website, you enter your username and password and click login. How the website _actually_ logs you in and how you get verified is all abstracted away from you (read more in `abstraction.md`).

### Encapsulation

Binding object state and behaviour togethe - creating classes is encapsulation. Another aspect of this is not allowing instance variables to be manipulated directly - generally instance variables should be **private**, and getters and setters should be used to manipulate these values.

```java
class EmployeeCount {
  private int numberOfEmployees = 0;

  public void setNoOfEmployees(int count) {
    numberOfEmployees = count;
  }

  public int getNoOfEmployees() {
    return numberOfEmployees;
  }
}
```

The behefit of encapsulation in java programming is that if implementation details of the class need to be changed in the future, this can be done so without affecting the classes that are using it.

### Inheritance

Process by which one class acquires the properties and functionalities of another class. Provides the idea of **reusability of code**, and each sub class defines only those features that are unique to it. The rest of the features can be inherited from the parent class.

1. Inheritance defines new class based on an existing class by extending its common data members and methods
2. Allows us to reuse code
3. Parent class is called the **base class** or **super class**. Child class that extends is called the derived class or **child class**

Biggest advantage of inheritance is that code in the base class does not need to be rewritten in the child class. All variables and metods of the base class can be used in the child class as well.

#### e.g. teacher and maths teacher

```java
class Teacher {
  String job = "Teacher";
  String college = "Imaginary School";
  
  void does() {
    System.out.println("Teaching");
  }
}

class MathsTeacher extends Teacher {
  String subject = "Maths";

  public static void main(String args[]) {
    MathsTeacher teacher = new MathsTeacher();
    System.out.println(teacher.job);
    System.out.println(teacher.college);
    System.out.println(teacher.subject);
    teacher.does();
    
    // Outputs:
    // Teacher
    // Imaginary School
    // Maths
    // Teaching
  }
}
```

We could create teachers of all different subjects that will all be able to access the variables and methods from the parent class, whilst only needing to define what subject they teach and any other unique behaviours/variables. This promotes reusability and limits the amount of code that needs to be written to achieve the same result.

#### Types of Inheritance

Multiple inheritance is **not allowed in Java**. This means that a class cannot inherit from two parent classes.

* Single inheritance - child and parent class relationship, where the child extends the parent class
* Multilevel inheritance - relationship where a class extends a child class. Therefore, class A extends class B and class B extends class C
* Hierarchical inheritance - more than one classes extend the same class, i.e. class B and class C _both_ extend class A

### Polymorphism

Allows us to perform a single action in different ways. e.g. a class `Animal`, which has a single method classed `animalSound()`. We can not give an implementation to this method, as we do not know which Animal class would extend the `Animal` parent class.

If we have two animal classes that extend the `Animal` class, then we can provide implementation details there.

```java
public abstract class Animal {
  ...
  public abstract void animalSound();
}

public class Lion extends Animal {
  public void animalSound() {
    System.out.println("Roar");
  }
}

public class Dog extends Animal {
  public void animalSound() {
    System.out.println("Woof");
  }
}
```

The two classes both have the same method, but the implementation is different for both.

#### Types of Polymorphism

**Static polymorphism** is resolved during compiler time. Method overloading is an example of this, which allows us to have more than one methods with the same name in a class that differs in signature (the return type must be the same). e.g.

```java
class DisplayOverloading {
  public void disp(char c) {
    System.out.println(c);
  }

  public void disp(char c, int i) {
    System.out.println(c + " " + i);
  }
}
```

**Dynamic Polymorphism** is a process in which a call to an overridden method is resolved at runtime. Also called runtime polymorphism, the `Animal` example above is an example of dynamic polymorphism.

## Abstract Class and methods in OOP

### Abstract methods

This is a method that is declared but not defined, only method signature and no body. Declared using the `abstract` keyword:

```java
public abstract void playInstrument();
```

It is used to put some kind of compulsion on any classes that inherits from the class with the abstract method. The class that inherits **must** provide the implementation of all abstract methods from the parent class.

### Abstract Class

A class that outlines the methods, but does not nexessarily implement all the methods. There can be some scenarios where it is difficult to implement all methods in a base class. A class derived from the abstract base class must implement those methods from the abstract class.

Abstract classes cannot be instantiated. To use the class, another class must be created that extends the abstract class to provide the implementation of abstract methods.

## Interfaces in Java

This is a blueprint of a class, which can be declared using the `interface` keyword. Interfaces cannot be instantiated, they can only be implemneted by classes or extended by other interfaces. This is a common way to achieve **full abstraction** in Java.

Whilst Java does not support multiple inheritance, a class can implement more than one interface. They are similar to an abstract class, but they only contain abstract methods. To implement an interface, use the `implements` keyword.

**See the `MathTeacher` directory for examples of this**

An interface may contain final variables and all methods are implicitly public and abstract. When a class implements an interface it has to give the definition of all abstract methods of interface.

### Generalisation and specialisation

To implement concept of inheritance in OOP, first must identify the similarities amongst different classes, to come up with the base class. This process is called generalisation.

In contract, specialisation means creating new subclasses from existing classes. If certain attributes or methods only apply to some objects of the class, a subclass can be created.
