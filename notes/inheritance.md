# Inheritance

The aim of inheritance is to provide reusability of code so that a class has to write only the unique features, the rest of the common methods and variables can be extended from another class.

## Constructors and Inheritance

When we create an object of a child class, the default constructor of the super class is also invoked. In inheritance, the objects are constructed top-down, meaning the parent class constructor is invoked first, and then the child classes.

The superclass constructor can be called explicitly using the `super` keyword, but it **should always be the first statement in a constructor**. Only the direct parent constructor can be accessed.

```java
class ParentClass {
  public ParentClass() {
    System.out.println("Constructor of Parent");
  }
}

class ChildClass extends ParentClass {
  public ChildClass() {
    System.out.println("Constructor of Child");
  }

  public static void main(String args[]) {
    new ChildClass();

    // output
    // Constructor of Parent
    // Constructor of Child
  }
}
```

By default, the unparameterized constructor of the parent class is called. If you want to call a parameterised constructor, this can be achived by explicitly calling `super(arg)`.

## Inheritance and Method Overriding

When a method is declared in a child class that is already present in the parent class, this is **method overriding** (note the difference between method _overloading_!) We can still call the parent class method using the `super` keyword.

```java
public class ParentClass {
  public ParentClass() {
    System.out.println("Constructor of Parent");
  }

  public void display() {
    System.out.println("Parent method");
  }
}

public class ChildClass extends ParentClass {
  public ChildClass() {
    System.out.println("Constructor of Child");
  }

  public void display() {
    System.out.println("Child method");
    // The next line will call the display method from the parent class
    super.display();
  }

  public static void main(String args[]) {
    ChildClass class = new ChildClass();

    class.display();

    // output
    // Constructor of Parent
    // Constructor of Child
    // Child method
    // Parent method
  }
}
```

## Why and When to use Inheritance

* Inheritance is useful for code reusability, as we are able to reuse attributes and methods of an existing class when creating new classes
* We can also increase features of classes or methods using overriding (runtime polymorphism can be achieved)
* **Is-A relationship** represents inheritance, and is implemented using the `extends` keyword
* **Has-A relationship** represents interfaces, and is implemented using `implements`

## Protected access modifier

This is an access modifier used for attributes, methods and constructors which is similar to `private`, but makes them accessible in the same package and subclasses.

## See the `InheritanceCarVehicle`, `InheritanceBank` and `InheritanceEmployee` directories for further examples
