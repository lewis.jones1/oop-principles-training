import java.util.ArrayList;

// Link to tutorial: https://www.cs.bham.ac.uk/~mdr/teaching/RedHotChilli/ex5A.html

public class Bank {
  ArrayList<Account> accounts = new ArrayList<>();

  // Adds an existing account to the bank
  public void addAccount(Account account) {
    accounts.add(account);
  }

  // Opens a basic account, returning the account
  public Account openAccount(int a) {
    Account account = new Account(a);
    accounts.add(account);
    return account;
  }

  // Opens a saving account, returning the account
  public SavingsAccount openSavingsAccount(int a, double interestRate) {
    SavingsAccount savingsAccount = new SavingsAccount(a, interestRate);
    accounts.add(savingsAccount);
    return savingsAccount;
  }

  // Opens a current account, returning the account
  public CurrentAccount openCurrentAccount(int a, double overdraftLimit) {
    CurrentAccount currentAccount = new CurrentAccount(a, overdraftLimit);
    accounts.add(currentAccount);
    return currentAccount;
  }

  // Iterates through all accounts in the bank
  // If saving account, adds interest
  // If current account is overdrawn, a message is printed to the console (simulates sending a letter)
  public void update() {
    for (Account account : accounts) {
      if (account instanceof SavingsAccount) ((SavingsAccount) account).addInterest();
      else if (account instanceof CurrentAccount) {
        if (account.getBalance() < 0) System.out.printf("Account number %d is overdrawn\n", account.getAccountNumber());
      }
    }
  }

  public static void main(String[] args) {
    Bank bank = new Bank();

    Account account = bank.openAccount(1);
    SavingsAccount savingsAccount = bank.openSavingsAccount(2, 1);
    CurrentAccount currentAccount = bank.openCurrentAccount(3, 100);

    account.deposit(10);
    savingsAccount.deposit(1000);
    currentAccount.withdraw(90);

    bank.update();
    bank.accounts.forEach(Account::print);
  }
}
