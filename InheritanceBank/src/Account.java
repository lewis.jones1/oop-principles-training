public class Account {
  private double balance;
  private final int accountNumber;

  public Account(int a) {
    this.balance = 0;
    this.accountNumber = a;
  }

  public double getBalance() {
    return balance;
  }

  public int getAccountNumber() {
    return accountNumber;
  }

  public void deposit(double amount) {
    if (amount > 0) {
      balance += amount;
    } else {
      System.err.println("Cannot deposit a negative amount");
    }
  }

  public void withdraw(double amount) {
    if (amount > 0) {
      balance -= amount;
    } else {
      System.err.println("Cannot withdraw a negative amount");
    }
  }

  public String toString() {
    return String.format("Account number: %d, balance: £%.2f", accountNumber, balance);
  }

  public final void print() {
    System.out.println(toString());
  }
}
