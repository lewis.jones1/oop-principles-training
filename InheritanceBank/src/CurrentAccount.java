public class CurrentAccount extends Account {
  private double overdraftLimit;

  public CurrentAccount(int a, double overdraftLimit) {
    super(a);
    this.overdraftLimit = overdraftLimit;
  }

  @Override
  public void withdraw(double amount) {
    if ((getBalance() - amount) >= -overdraftLimit) {
      super.withdraw(amount);
    } else {
      System.err.println("Cannot withdraw that amount");
    }
  }

  public String toString() {
    return String.format("Account number: %d, balance: £%.2f, overdraft limit: %.2f", getAccountNumber(), getBalance(), overdraftLimit);
  }
}
