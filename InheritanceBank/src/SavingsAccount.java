public class SavingsAccount extends Account {
  private double interestRate;

  public SavingsAccount(int a, double interestRate) {
    super(a);
    this.interestRate = interestRate;
  }

  public void addInterest() {
    deposit(getBalance() * (interestRate/100));
  }

  public void setInterestRate(double rate) {
    interestRate = rate;
  }

  public String toString() {
    return String.format("Account number: %d, balance: £%.2f, interest rate: %.2f%%", getAccountNumber(), getBalance(), interestRate);
  }
}
