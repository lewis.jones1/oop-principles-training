public class DiscountRate {
  private final static double serviceDiscountPremium = 0.2;
  private final static double serviceDiscountGold = 0.15;
  private final static double serviceDiscountSilver = 0.1;

  private final static double productDiscountPremium = 0.1;
  private final static double productDiscountGold = 0.1;
  private final static double productDiscountSilver = 0.1;

  public static double getServiceDiscountRate(String type) {
    return (type != null) ? getRate(type, serviceDiscountPremium, serviceDiscountGold, serviceDiscountSilver) : 0;
  }

  public static double getProductDiscountRate(String type) {
    return (type != null) ? getRate(type, productDiscountPremium, productDiscountGold, productDiscountSilver) : 0;
  }

  private static double getRate(String type, double premium, double gold, double silver) {
    double rate;
    switch (type) {
      case "premium":
        rate = premium;
        break;
      case "gold":
        rate = gold;
        break;
      case "silver":
        rate = silver;
        break;
      default:
        rate = 0;
    }
    return rate;
  }
}
