import java.util.Date;

public class Visit {
  private Customer customer;
  private Date date;
  private double serviceExpense;
  private double productExpense;

  public Visit(Customer customer, Date date) {
    this.date = date;
    this.customer = customer;
  }

  public String getName() {
    return customer.getName();
  }

  public double getServiceExpense() {
    return serviceExpense;
  }

  public void setServiceExpense(double serviceExpense) {
    this.serviceExpense = serviceExpense;
  }

  public double getProductExpense() {
    return productExpense;
  }

  public void setProductExpense(double productExpense) {
    this.productExpense = productExpense;
  }

  public double getTotalExpense() {
    double serviceDiscountRate = DiscountRate.getServiceDiscountRate(customer.getMemberType());
    double productDiscountRate = DiscountRate.getProductDiscountRate(customer.getMemberType());

    return (serviceExpense - (serviceExpense * serviceDiscountRate) + productExpense - (productExpense * productDiscountRate));
  }

  public String toString() {
    return String.format("Customer: %s, Service cost: £%.2f, Product cost: £%.2f\nTotal (before discount): £%.2f\nTotal: £%.2f\n",
    customer.getName(), serviceExpense, productExpense, (serviceExpense + productExpense), getTotalExpense());
  }
}
