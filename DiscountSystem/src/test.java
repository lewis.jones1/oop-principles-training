import java.util.Date;

public class test {

  public static void main(String[] args) {
    Customer lewis = new Customer("Lewis");
    lewis.setMember(true);
    lewis.setMemberType("premium");

    Visit v1 = new Visit(lewis, new Date());
    v1.setServiceExpense(80);
    v1.setProductExpense(100);
    System.out.println(v1);

    Customer brian = new Customer("Brian");
    brian.setMember(false);

    Visit v2 = new Visit(brian, new Date());
    v2.setServiceExpense(120);
    v2.setProductExpense(350);
    System.out.println(v2);

    Customer bruce = new Customer("Bruce");
    bruce.setMember(true);
    bruce.setMemberType("silver");

    Visit v3 = new Visit(bruce, new Date());
    v3.setServiceExpense(100);
    v3.setProductExpense(250);
    System.out.println(v3);
  }
}
