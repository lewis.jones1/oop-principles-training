public class Customer extends Person {
  private boolean member;
  private String memberType;

  public Customer(String name) {
    super(name);
  }

  public Customer(String name, boolean member, String memberType) {
    super(name);
    this.member = member;
    this.memberType = memberType;
  }

  public boolean isMember() {
    return member;
  }

  public void setMember(boolean member) {
    this.member = member;
  }

  public String getMemberType() {
    return memberType;
  }

  public void setMemberType(String memberType) {
    if (member) {
      this.memberType = memberType;
    } else {
      System.err.println("Must be a member before assigning membership type");
    }
  }

  public String toString() {
    return String.format("Name: %s, Member: %s, Membership: %s", getName(), member, memberType);
  }
}
