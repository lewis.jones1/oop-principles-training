public class Employee extends Person {
  private double salary;
  private int startYear;
  private String nino;

  // full constructor with all details supplied
  public Employee(String name, double salary, int startYear, String nino) {
    super(name);
    this.salary = salary;
    this.startYear = startYear;
    this.nino = nino;
  }

  // Only name supplied
  public Employee(String name) {
    super(name);
  }

  public double getSalary() {
    return salary;
  }

  public void setSalary(double salary) {
    this.salary = salary;
  }

  public int getStartYear() {
    return startYear;
  }

  public void setStartYear(int startYear) {
    this.startYear = startYear;
  }

  public String getNino() {
    return nino;
  }

  public void setNino(String nino) {
    this.nino = nino;
  }

  public void print() {
    System.out.printf("Name: %s, Started: %d, Nino: %s, Salary: £%.2f\n",
        getName(), getStartYear(), getNino(), getSalary());
  }

  public static void main(String[] args) {
    Employee e1 = new Employee("Lewis");
    e1.print();

    e1.setNino("ASDSDS");
    e1.setStartYear(2020);
    e1.setSalary(1250.55);

    e1.print();

    Employee e2 = new Employee("Brian", 10000, 2010, "dssds");
    e2.print();
  }
}
