public class Teacher {
  private String name;
  private double salary;
  private String mainSubject;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getSalary() {
    return salary;
  }

  public void setSalary(double salary) {
    this.salary = salary;
  }

  public String getMainSubject() {
    return mainSubject;
  }

  public void setMainSubject(String mainSubject) {
    this.mainSubject = mainSubject;
  }
}
