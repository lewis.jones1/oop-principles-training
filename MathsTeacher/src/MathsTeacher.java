public class MathsTeacher extends Teacher implements Mathematician {

  @Override
  public void solveMathProblem() {
    // Implementing the interface method
    System.out.println("I solved the maths problem!");
  }

  public static void main(String[] args) {
    MathsTeacher teacher = new MathsTeacher();
    // These methods are from the parent Teacher class
    teacher.setName("Lewis");
    teacher.setMainSubject("Maths");
    teacher.setSalary(1000);
    System.out.printf("I am %s, I teach %s and I get paid £%.2f\n", teacher.getName(), teacher.getMainSubject(), teacher.getSalary());

    // Using the method from MathsTeacher, which is the implementation of the interface
    teacher.solveMathProblem();
  }
}
