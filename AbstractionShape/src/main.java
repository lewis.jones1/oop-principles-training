public class main {
  public static void main(String[] args) {
//    Shape triangle = new RtTriangle(20, 10);
//    Shape rectangle = new Rectangle(15, 12);
//
//    System.out.println(triangle.getArea());
//    System.out.println(triangle.getPerimeter());
//
//    System.out.println(rectangle.getArea());
//    System.out.println(rectangle.getPerimeter());

    Rectangle r = new Rectangle(2,3);
    System.out.println(r.getArea());
    System.out.println(r.getPerimeter());

    System.out.println("Resizing....");
    r.resize(2);

    System.out.println(r.getArea());
    System.out.println(r.getPerimeter());
  }
}
