public interface Moves {
  // The move method could apply to any vehicle, but could also apply to animals etc.
  public abstract void move();
}
