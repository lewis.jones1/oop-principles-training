public class Car extends Vehicle implements Moves {
  // not all vehicles have wheels, so this is unique
  // using encapsulation by declaring variable private and providing getters and setters
  private int numberOfWheels;

  public int getNumberOfWheels() {
    return numberOfWheels;
  }

  public void setNumberOfWheels(int numberOfWheels) {
    this.numberOfWheels = numberOfWheels;
  }

  public void move() {
    // implements the move method from the moves interface
    System.out.printf("I'm driving on my %d wheels!", this.getNumberOfWheels());
  }

  public static void main(String[] args) {
    Car car = new Car();
    // using methods from the vehicle superclass
    car.setBrand("Volkswagen");
    car.setModel("Golf");
    // Using method from car child class
    car.setNumberOfWheels(4);

    System.out.printf("%s %s\n", car.getBrand(), car.getModel());
    // using method that was implemented in this class, from the Moves interface
    car.move();
  }
}
