# Object Oriented Programming Principles

I will use this repository to keep track of any notes made during my self-guided study days, focussed on OOP principles. I will also include code examples from any tutorials completed in appropriately named directories.

## References

1. [Beginner's Book - OOPs concepts in Java](https://beginnersbook.com/2013/04/oops-concepts/)
